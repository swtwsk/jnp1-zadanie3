#include <iostream>
#include <cassert>
#include <algorithm>
#include "../sejf.h"

using namespace std;

void test(Sejf &s, int ile){
	for(int i = 0; i < ile; i++){
		assert(s[0] == 'a');
	}
	for(int i = 0; i < 10; i++){
		assert(s[i] == -1);
	}
}
void test_fb() {
    Sejf s1("aaa", 2);
    assert(s1[0] == 'a');
    (s1 *= 3) -= 1;
    assert(s1[0] == 'a');
    assert(s1[0] == 'a');
    assert(s1[0] == -1);
}
void test_fb2() {
     Sejf s6("zawartosc", 1);
    Sejf s7("zawartosc", 0);
    auto k2 = s6.kontroler();
    swap(s6, s7);
    assert(!k2);
}
void test_fb3() {   
    string blah = "new";
    Sejf s1(blah, 2);
    assert(s1[1] == 'e');
    blah[1] = 'o';
    cout << blah << '\n';
    assert(s1[1] == 'e');
    assert(s1[1] == -1);
    assert(s1[2] == -1);
}
void test_moodle() {
    Sejf s1("zawartosc", 1);
    Sejf s2("zawartosc", 0);
    auto k = s1.kontroler();
    swap(s1, s2);
    
    string res;
    
    if (k) {
        res = "TAK";
    } else {
        res = "NIE";
    }
    assert(res == "NIE");
}
void test_tresc1() {
    Sejf s("a\xff", 3);
    assert(s[0] == 'a');
    assert(s[1] == 255);
    assert(s[1] == (int)((unsigned char) -1));
    assert(s[1] == -1);
    assert(s[1] != (int)((unsigned char) -1));
}

void test_tresc2() {
    Sejf s1("aaa", 2);
    s1[0];
    s1 *=3;
    
    assert(s1[1] == 'a');
    assert(s1[1] == 'a');
    assert(s1[1] == 'a');
    assert(s1[1] == (-1));
}
void test_blad_kompilacji() {
    Sejf s1("aaa", 2);
   // Sejf s4 = s1;
    //Sejf s5(s1);
    //s6 = s1;
}

void test_blad_kompilacji2() {
    /*Sejf s1("aaa", 1);
    Sejf s2("bbb", 2);
    
    if(s1.kontroler() < s2.kontroler()){
        cout << "LOL" << endl;
    }*/
}

void test_tresc3() {
    Sejf s1("aaa", 2);
    s1[2];
    auto k1 = s1.kontroler();
    cout << k1 << "test\n";
    s1[2];
    s1[3];
    s1[4];
    cout << k1;
    s1[3];
    cout << k1;
    s1[2];
    cout << k1;
}
void test_tresc4() {
    Sejf s1("aaa", 2);
    auto k = s1.kontroler();
    while (k) {
        assert(s1[1] != (-1));
    }
}

void test_moj() {
    Sejf s("abc", 5);
	assert(s[0] == 'a');
	assert(s[1] == 'b');
	assert(s[2] == 'c');
	assert(s[1] == 'b');
	assert(s[0] == 'a');
	for(int i = 0; i < 10; i++){
		assert(s[i] == -1);
	}
	s += 5;
	test(s, 5);
	s += 2;
	s *= 3;
	test(s, 2*3);

	s += 10;
	s -= 5;
	test(s, 10-5);

	//s += (-3);
	//test(s, 0);

	//s -= (-4);
	//test(s, 0);

	s += 2;
	//s *= (-1);
	test(s, 2);

	s += 5;
	s *= 0;
	test(s, 5);
}

void testSwap() {
    Sejf a("ssij", 2);
    Sejf b("pauke", 3);

    a += 1;
    b[0];
    b[0];
    b[0];

    auto ak = a.kontroler();
    auto bk = b.kontroler();

    cout << ak << bk << "---" << endl;
    std::swap(a, b);

    cout << ak << bk << "---" << endl;
}

int main() {
    test_moodle();
    test_tresc1();
    test_tresc2();
    test_tresc3();
    test_tresc4();
    test_moj();

    test_fb();
    test_fb2();
    test_fb3();

    testSwap();

    return 0;

}
