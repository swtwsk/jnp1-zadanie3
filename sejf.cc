#include <iostream>
#include <algorithm>

#include "sejf.h"

using std::string;
using AccessCountType = unsigned long long;

Sejf::Sejf(const string& napis, AccessCountType liczba) 
    : napis(napis), liczba(liczba), wlamanie(false), manipulacja(false) {}

Sejf::Sejf(Sejf&& s) 
    : napis(std::move(s.napis)), liczba(s.liczba), wlamanie(s.wlamanie), manipulacja(s.manipulacja) {}

Sejf& Sejf::operator=(Sejf&& s) {
    if (this != &s) {
        napis = std::move(s.napis);
        liczba = s.liczba;
        wlamanie = s.wlamanie;
        manipulacja = s.manipulacja;
    }

    return *this;
}

int16_t Sejf::operator[](std::size_t indeks) const {
    if (indeks >= napis.size()) {
        return -1;
    }

    if (liczba > 0) {
        --liczba;
    }
    else {
        wlamanie = true;
        return -1;
    }

    unsigned char c = napis[indeks];
    return c;
}

Sejf& Sejf::operator+=(AccessCountType rhs) {
    AccessCountType nowa_liczba = liczba + rhs;

    if (nowa_liczba >= liczba) {
        liczba += rhs;
        manipulacja = true;
    }

    return *this;
}

Sejf& Sejf::operator-=(AccessCountType rhs) {
    AccessCountType nowa_liczba = liczba - rhs;

    if (nowa_liczba <= liczba) {
        liczba -= rhs;
        manipulacja = true;
    }

    return *this;
}

Sejf& Sejf::operator*=(AccessCountType rhs) {
    if (rhs > 0) {
        AccessCountType nowa_liczba = liczba * rhs;

        if (nowa_liczba >= liczba) {
            liczba *= rhs;
            manipulacja = true;
        }
    }

    return *this;
}

const Kontroler Sejf::kontroler() const {
    return Kontroler(*this);
}
