#ifndef JNP1_ZADANIE3_KONTROLER_H
#define JNP1_ZADANIE3_KONTROLER_H

#include <ostream>

class Sejf;

class Kontroler {
    friend class Sejf;
public:
    friend std::ostream& operator<<(std::ostream& os, const Kontroler& kontroler);
    explicit operator bool() const;

    Kontroler(const Kontroler& k) = delete;
    Kontroler(Kontroler&& k) = delete;

    Kontroler& operator=(const Kontroler& k) = delete;
    Kontroler& operator=(Kontroler&& k) = delete;

private:
    Kontroler(const Sejf& sejf);

    const Sejf& sejf;
};

#endif //JNP1_ZADANIE3_KONTROLER_H