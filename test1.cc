#include <iostream>
#include <cassert>

#include "sejf.h"

using namespace std;

void testForum() {
    Sejf s1("zawartosc", 1);
    Sejf s2("zawartosc", 0);
    auto k = s1.kontroler();
    swap(s1, s2);

    assert(!k);

    cout << "Forum test PASSED" << endl;
}

void boolTest() {
    Sejf s0("tata", 0);
    assert(!s0.kontroler());

    Sejf s1("mama", 2);
    assert(s1.kontroler());
    assert(s1[0] == (int)'m');
    assert(s1.kontroler());
    assert(s1[4] == -1);
    assert(s1.kontroler());
    assert(s1[1] == (int)'a');
    assert(!s1.kontroler());

    Sejf s2("babcia", 0);
    auto k2 = s2.kontroler();
    assert(!k2);

    cout << "Bool test PASSED" << endl;
}

void defaultTest() {
    Sejf table("James Hetfield");
    for (int i = 0; i < 42; i++) {
        assert((unsigned char)table[0] == 'J');
    }
    assert(table[0] == -1);

    cout << "Default test PASSED" << endl;
}

void balcerzakTest() {
    Sejf s1("aaa", 2);
    assert(s1[0] == 'a');
    (s1 *= 3) -= 1;
    assert(s1[0] == 'a');
    assert(s1[0] == 'a');
    assert(s1[0] == -1);

    cout << "Balcerzak test PASSED" << endl;
}

void bazinskaTest() {
    Sejf s6("zawartosc", 1);
    Sejf s7("zawartosc", 0);
    auto k2 = s6.kontroler();
    swap(s6, s7);
    assert(!k2);

    cout << "Bazinska test PASSED" << endl;
}

void coSieStanelo() {
    Sejf s("mama", 3);
    auto k = s.kontroler();

    cout << k;
    s[0];
    s[0];
    cout << k;
    s[0];
    cout << k;
    s[0];
    cout << k;
}

int main() {
    Sejf s1("aaa", 2);
    auto k1 = s1.kontroler();
    cout << k1 << "test\n";
    s1[2];
    s1[2];
    s1[4];
    cout << k1;
    s1[3];
    cout << k1;
    s1[2];
    cout << k1;

    cout << endl;

    cout << "Czy cos sie popsulo? :c" << endl;
    coSieStanelo();

    testForum();
    boolTest();
    defaultTest();
    balcerzakTest();
    bazinskaTest();
}
