CC=g++
CCFLAGS=-Wall -Wextra -O2 -std=c++17

all: test1

test1: sejf.o kontroler.o
	$(CC) -c $(CCFLAGS) testy/test1.cc -o test1.o
	$(CC) test1.o sejf.o kontroler.o -o test1

test2: sejf.o kontroler.o
	$(CC) -c $(CCFLAGS) testy/main.cc -o main.o
	$(CC) main.o sejf.o kontroler.o -o main

test3: sejf.o kontroler.o
	$(CC) -c $(CCFLAGS) testy/test.cc -o test.o
	$(CC) test.o sejf.o kontroler.o -o test

sejf.o:
	$(CC) -c $(CCFLAGS) sejf.cc -o sejf.o

kontroler.o:
	$(CC) -c $(CCFLAGS) kontroler.cc -o kontroler.o

clean:
	rm -f *.o test1
