#include "kontroler.h"
#include "sejf.h"

Kontroler::Kontroler(const Sejf& sejf) : sejf(sejf) {}

std::ostream &operator<<(std::ostream& os, const Kontroler& kontroler) {
    if (kontroler.sejf.wlamanie)
        os << "ALARM: WLAMANIE\n";
    else if (kontroler.sejf.manipulacja)
        os << "ALARM: ZMANIPULOWANY\n";
    else
        os << "OK\n";

    return os;
}

Kontroler::operator bool() const {
    return sejf.liczba > 0;
}