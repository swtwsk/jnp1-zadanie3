#ifndef JNP1_ZADANIE3_SEJF_H
#define JNP1_ZADANIE3_SEJF_H

#include <cstddef>
#include <string>

#include "kontroler.h"

class Sejf {
    friend class Kontroler;

public:
    Sejf(const std::string& napis, unsigned long long liczba = MAX_DOSTEPOW);

    Sejf(const Sejf& s) = delete;
    Sejf& operator=(const Sejf& s) = delete;

    Sejf(Sejf&& s);
    Sejf& operator=(Sejf&& s);

    int16_t operator[](std::size_t indeks) const;
    Sejf& operator+=(unsigned long long rhs);
    Sejf& operator-=(unsigned long long rhs);
    Sejf& operator*=(unsigned long long rhs);

    const Kontroler kontroler() const;
    friend std::ostream& operator<<(std::ostream& os, const Kontroler& kontroler);

private:
    const static unsigned long long MAX_DOSTEPOW = 42;

    std::string napis;
    mutable unsigned long long liczba;

    mutable bool wlamanie;
    mutable bool manipulacja;
};

#endif  //JNP1_ZADANIE3_SEJF_H